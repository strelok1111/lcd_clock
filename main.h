#ifndef MAIN_H_
#define MAIN_H_

#define STM32F1
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/rtc.h>
#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/f1/bkp.h>
#include <stdint.h>
#include <string.h>

#define REGISTER_PORT GPIOB
#define COM_1_2_PORT GPIOB
#define LATCH_PIN GPIO13
#define DATA_CLK_PIN GPIO14
#define REGISTER_DATA GPIO12

#define COM_1_0_PIN GPIO6
#define COM_1_1_PIN GPIO7
#define COM_2_0_PIN GPIO8
#define COM_2_1_PIN GPIO9

#define TOUCH_LATCH gpio_set(REGISTER_PORT,LATCH_PIN);gpio_clear(REGISTER_PORT,LATCH_PIN)
#define TOUCH_DATA gpio_set(REGISTER_PORT,DATA_CLK_PIN);gpio_clear(REGISTER_PORT,DATA_CLK_PIN)

#define COM_1_V0 gpio_clear(COM_1_2_PORT,COM_1_0_PIN | COM_1_1_PIN)
#define COM_1_V1 gpio_clear(COM_1_2_PORT,COM_1_1_PIN);gpio_set(COM_1_2_PORT,COM_1_0_PIN)
#define COM_1_V2 gpio_clear(COM_1_2_PORT,COM_1_0_PIN);gpio_set(COM_1_2_PORT,COM_1_1_PIN)
#define COM_1_V3 gpio_set(COM_1_2_PORT,COM_1_0_PIN | COM_1_1_PIN)

#define COM_2_V0 gpio_clear(COM_1_2_PORT,COM_2_0_PIN | COM_2_1_PIN)
#define COM_2_V1 gpio_clear(COM_1_2_PORT,COM_2_1_PIN);gpio_set(COM_1_2_PORT,COM_2_0_PIN)
#define COM_2_V2 gpio_clear(COM_1_2_PORT,COM_2_0_PIN);gpio_set(COM_1_2_PORT,COM_2_1_PIN)
#define COM_2_V3 gpio_set(COM_1_2_PORT,COM_2_0_PIN | COM_2_1_PIN)

typedef	uint16_t			u16_t;
typedef	uint32_t			u32_t;
typedef	int16_t			s16_t;
typedef	int32_t			s32_t;

//#define LAST_FULL_MOON 1484220840
//#define MOON_CICLE_SEK 2551442

typedef	u32_t  xtime_t;
typedef uint16_t Coord;

struct tm {
	int8_t	tm_sec;
	int8_t	tm_min;
	int8_t	tm_hour;
	int8_t	tm_mday;
	int8_t	tm_wday;
	int8_t	tm_mon;
	u16_t	tm_year;
};
#define _TBIAS_DAYS		((70 * (u32_t)365) + 17)
#define _TBIAS_SECS		(_TBIAS_DAYS * (xtime_t)86400)
#define	_TBIAS_YEAR		1900
#define MONTAB(year)	((((year) & 03) || ((year) == 0)) ? mos : lmos)

const s16_t	lmos[] = {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335};
const s16_t	mos[] = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};

#define	Daysto32(year, mon)	(((year - 1) / 4) + MONTAB(year)[mon])
void xttotm(struct tm *, xtime_t);
static void set_frame_seg(void);
static void set_dots(void);
static void set_date_slash(void);
static uint16_t get_temp_raw(void);
static void usart_setup(void);
static void send_string(uint8_t *);
static float get_temp(void);

#endif /* MAIN_H_ */
