#include "main.h"
volatile uint8_t seg_data_enabled[29] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
volatile uint8_t current_frame_seg = 0;
volatile uint8_t recalculate_screen = 0;
volatile float temp = 0;

uint16_t hour_1[][3] = {
		{0},                                   //0 Dummy
		{0b00011},                             //1
		{0b00110}                              //2
};

uint16_t hour_0[][3] = {
		{15 << 5 | 0b11011,16 << 5 | 0b01010}, //0
		{16 << 5 | 0b01010,0},                 //1
		{15 << 5 | 0b10111,16 << 5 | 0b01000}, //2
		{15 << 5 | 0b10101,16 << 5 | 0b01010}, //3
		{15 << 5 | 0b01100,16 << 5 | 0b01010}, //4
		{15 << 5 | 0b11101,16 << 5 | 0b00010}, //5
		{15 << 5 | 0b11111,16 << 5 | 0b00010}, //6
		{15 << 5 | 0b10000,16 << 5 | 0b01010}, //7
		{15 << 5 | 0b11111,16 << 5 | 0b01010}, //8
		{15 << 5 | 0b11101,16 << 5 | 0b01010}, //9
};

uint16_t minute_0[][3] = {
		{18 << 5 | 0b11010,19 << 5 | 0b01011}, //0
		{19 << 5 | 0b01010,0},                 //1
		{18 << 5 | 0b10010,19 << 5 | 0b01101}, //2
		{18 << 5 | 0b10000,19 << 5 | 0b01111}, //3
		{18 << 5 | 0b01000,19 << 5 | 0b01110}, //4
		{18 << 5 | 0b11000,19 << 5 | 0b00111}, //5
		{18 << 5 | 0b11010,19 << 5 | 0b00111}, //6
		{18 << 5 | 0b10000,19 << 5 | 0b01010}, //7
		{18 << 5 | 0b11010,19 << 5 | 0b01111}, //8
		{18 << 5 | 0b11000,19 << 5 | 0b01111}, //9
};

uint16_t minute_1[][3] = {
		{17 << 5 | 0b11011,18 << 5 | 0b00101}, //0
		{18 << 5 | 0b00101},                   //1
		{17 << 5 | 0b10111,18 << 5 | 0b00100}, //2
		{17 << 5 | 0b10101,18 << 5 | 0b00101}, //3
		{17 << 5 | 0b01100,18 << 5 | 0b00101}, //4
		{17 << 5 | 0b11101,18 << 5 | 0b00001}, //5
		{17 << 5 | 0b11111,18 << 5 | 0b00001}, //6
		{17 << 5 | 0b10000,18 << 5 | 0b00101}, //7
		{17 << 5 | 0b11111,18 << 5 | 0b00101}, //8
		{17 << 5 | 0b11101,18 << 5 | 0b00101}, //9
};

uint16_t second_1[][3] = {
		{20 << 5 | 0b00111,21 << 5 | 0b00101,22 << 5 | 0b00010}, //0
		{20 << 5 | 0b00000,21 << 5 | 0b00100,22 << 5 | 0b00010}, //1
		{20 << 5 | 0b00101,21 << 5 | 0b00111},                   //2
		{20 << 5 | 0b00100,21 << 5 | 0b00111,22 << 5 | 0b00010}, //3
		{20 << 5 | 0b00010,21 << 5 | 0b00110,22 << 5 | 0b00010}, //4
		{20 << 5 | 0b00110,21 << 5 | 0b00011,22 << 5 | 0b00010}, //5
		{20 << 5 | 0b00111,21 << 5 | 0b00011,22 << 5 | 0b00010}, //6
		{20 << 5 | 0b00100,21 << 5 | 0b00100,22 << 5 | 0b00010}, //7
		{20 << 5 | 0b00111,21 << 5 | 0b00111,22 << 5 | 0b00010}, //8
		{20 << 5 | 0b00110,21 << 5 | 0b00111,22 << 5 | 0b00010}  //9
};

uint16_t second_0[][3] = {
		{22 << 5 | 0b00101,23 << 5 | 0b11011}, //0
		{23 << 5 | 0b01010},                   //1
		{22 << 5 | 0b00001,23 << 5 | 0b11101}, //2
		{23 << 5 | 0b11111},                   //3
		{22 << 5 | 0b00100,23 << 5 | 0b01110}, //4
		{22 << 5 | 0b00100,23 << 5 | 0b10111}, //5
		{22 << 5 | 0b00101,23 << 5 | 0b10111}, //6
		{23 << 5 | 0b11010},                   //7
		{22 << 5 | 0b00101,23 << 5 | 0b11111}, //8
		{22 << 5 | 0b00100,23 << 5 | 0b11111}, //9
};

uint16_t day_1[][3] = {
		{0},                                  //0
		{1 << 5 | 0b00110},                   //1
		{1 << 5 | 0b11010},                   //2
		{1 << 5 | 0b01110},                   //3
};

uint16_t day_0[][3] = {
		{2 << 5 | 0b11011,3 << 5 | 0b10100}, //0
		{3 << 5 | 0b10100},                  //1
		{2 << 5 | 0b11101,3 << 5 | 0b00100}, //2
		{2 << 5 | 0b10101,3 << 5 | 0b10100}, //3
		{2 << 5 | 0b00110,3 << 5 | 0b10100}, //4
		{2 << 5 | 0b10111,3 << 5 | 0b10000}, //5
		{2 << 5 | 0b11111,3 << 5 | 0b10000}, //6
		{2 << 5 | 0b00001,3 << 5 | 0b10100}, //7
		{2 << 5 | 0b11111,3 << 5 | 0b10100}, //8
		{2 << 5 | 0b10111,3 << 5 | 0b10100}, //9
};

uint16_t month_1[][3] = {
		{0},                                  //0
		{4 << 5 | 0b00100},                   //1
};

uint16_t month_0[][3] = {
		{4 << 5 | 0b11011,5 << 5 | 0b00101}, //0
		{5 << 5 | 0b00101},                  //1
		{4 << 5 | 0b11001,5 << 5 | 0b00011}, //2
		{4 << 5 | 0b10001,5 << 5 | 0b00111}, //3
		{4 << 5 | 0b00010,5 << 5 | 0b00111}, //4
		{4 << 5 | 0b10011,5 << 5 | 0b00110}, //5
		{4 << 5 | 0b11011,5 << 5 | 0b00110}, //6
		{4 << 5 | 0b00001,5 << 5 | 0b00101}, //7
		{4 << 5 | 0b11011,5 << 5 | 0b00111}, //8
		{4 << 5 | 0b10011,5 << 5 | 0b00111}, //9
};

uint16_t temp_2[][3] = {
		{0},                                   //0
		{25 << 5 | 0b01000},                   //1
};

uint16_t temp_1[][3] = {
		{24 << 5 | 0b11011,25 << 5 | 0b00101}, //0
		{24 << 5 | 0b00000,25 << 5 | 0b00101}, //1
		{24 << 5 | 0b11101,25 << 5 | 0b00001}, //2
		{24 << 5 | 0b10101,25 << 5 | 0b00101}, //3
		{24 << 5 | 0b10100,25 << 5 | 0b00101}, //4
		{24 << 5 | 0b10111,25 << 5 | 0b00100}, //5
		{24 << 5 | 0b11111,25 << 5 | 0b00100}, //6
		{24 << 5 | 0b00001,25 << 5 | 0b00101}, //7
		{24 << 5 | 0b11111,25 << 5 | 0b00101}, //8
		{24 << 5 | 0b10111,25 << 5 | 0b00101}, //9
};

uint16_t temp_0[][3] = {
		{25 << 5 | 0b01010,26 << 5 | 0b11011}, //0
		{26 << 5 | 0b01010},                   //1
		{25 << 5 | 0b01000,26 << 5 | 0b10111}, //2
		{26 << 5 | 0b11111},                   //3
		{25 << 5 | 0b00010,26 << 5 | 0b01110}, //4
		{25 << 5 | 0b00010,26 << 5 | 0b11101}, //5
		{25 << 5 | 0b01010,26 << 5 | 0b11101}, //6
		{26 << 5 | 0b01011},                   //7
		{25 << 5 | 0b01010,26 << 5 | 0b11111}, //8
		{25 << 5 | 0b00010,26 << 5 | 0b11111}, //9
};

uint16_t temp_d[][3] = {
		{27 << 5 | 0b11110,28 << 5 | 0b10100}, //0
		{28 << 5 | 0b10100},                   //1
		{27 << 5 | 0b11010,28 << 5 | 0b01100}, //2
		{27 << 5 | 0b10010,28 << 5 | 0b11100}, //3
		{27 << 5 | 0b00100,28 << 5 | 0b11100}, //4
		{27 << 5 | 0b10110,28 << 5 | 0b11000}, //5
		{27 << 5 | 0b11110,28 << 5 | 0b11000}, //6
		{27 << 5 | 0b00010,28 << 5 | 0b10100}, //7
		{27 << 5 | 0b11110,28 << 5 | 0b11100}, //8
		{27 << 5 | 0b10110,28 << 5 | 0b11100}, //9
};

void xttotm(struct tm *t, xtime_t secsarg){
	u32_t		secs;
	s32_t		days;
	s32_t		mon;
	s32_t		year;
	s32_t		i;
	const s16_t*	pm;

	secs = secsarg + 3600 * 3;
	days = _TBIAS_DAYS;

	days += secs / 86400;
	secs = secs % 86400;
	t->tm_wday = days % 7;
	t->tm_hour = secs / 3600;
	secs %= 3600;
	t->tm_min = secs / 60;
	t->tm_sec = secs % 60;

	for (year = days / 365; days < (i = Daysto32(year, 0) + 365*year); ) { --year; }
	days -= i;
	t->tm_year = year + _TBIAS_YEAR;

	pm = MONTAB(year);
	for (mon = 12; days < pm[--mon]; );
	t->tm_mon = mon + 1;
	t->tm_mday = days - pm[mon] + 1;
}

static void delay_ms(uint32_t ms){
	uint32_t delay = ms * 9000;
	for(uint32_t i = 0; i< delay;i++)__asm__("nop");
}

static void timer_setup(void){
	rcc_periph_clock_enable(RCC_TIM3);
	nvic_enable_irq(NVIC_TIM3_IRQ);
	nvic_set_priority(NVIC_TIM3_IRQ, 3);
	rcc_periph_reset_pulse(RST_TIM3);
	timer_set_prescaler(TIM3, 499);
	timer_set_period(TIM3, 287);
	timer_enable_counter(TIM3);
	timer_enable_irq(TIM3, TIM_DIER_UIE);
}

void tim3_isr(void){
	if (timer_get_flag(TIM3, TIM_SR_UIF)) {
		timer_clear_flag(TIM3, TIM_SR_UIF);
		set_frame_seg();
	}
}

static void rtc_setup(void){

	nvic_enable_irq(NVIC_RTC_IRQ);
	nvic_set_priority(NVIC_RTC_IRQ, 1);
	rtc_interrupt_enable(RTC_SEC);
	rtc_auto_awake(RCC_LSE, 0x7fff);
	rtc_enter_config_mode();

	BKP_RTCCR |= 67;
	rtc_exit_config_mode();
}

void rtc_isr(void){
	recalculate_screen = 1;
	rtc_clear_flag(RTC_SEC);
}

static void gpio_setup(void){
	rcc_periph_clock_enable(RCC_GPIOB);
	gpio_set_mode(REGISTER_PORT, GPIO_MODE_OUTPUT_2_MHZ,GPIO_CNF_OUTPUT_PUSHPULL,LATCH_PIN | DATA_CLK_PIN | REGISTER_DATA);
	gpio_clear(REGISTER_PORT,LATCH_PIN | REGISTER_DATA);
	gpio_set_mode(COM_1_2_PORT, GPIO_MODE_OUTPUT_2_MHZ,GPIO_CNF_OUTPUT_PUSHPULL,COM_1_0_PIN | COM_1_1_PIN | COM_2_0_PIN | COM_2_1_PIN);
	gpio_clear(COM_1_2_PORT,COM_1_0_PIN | COM_1_1_PIN | COM_2_0_PIN | COM_2_1_PIN);
}

static void write_to_registers(uint32_t data2,uint32_t data1){
	for(uint8_t i = 0;i < 32;i++){
		if(data2 & ( 1 << (31 - i) )){
			gpio_set(REGISTER_PORT,REGISTER_DATA);
		}else{
			gpio_clear(REGISTER_PORT,REGISTER_DATA);
		}
		TOUCH_DATA;
	}
	for(uint8_t i = 0;i < 32;i++){
		if(data1 & ( 1 << (31 - i) )){
			gpio_set(REGISTER_PORT,REGISTER_DATA);
		}else{
			gpio_clear(REGISTER_PORT,REGISTER_DATA);
		}
		TOUCH_DATA;
	}
	TOUCH_LATCH;
}
static void set_frame_seg(void){
	uint32_t data = 0;
	uint32_t data2 = 0;
	if(current_frame_seg == 0){
		data = 0b1010101000;
	}else if(current_frame_seg == 1){
		data = 0b0101010111;
	}else if(current_frame_seg == 2){
		data = 0b1010100010;
	}else if(current_frame_seg == 3){
		data = 0b0101011101;
	}else if(current_frame_seg == 4){
		data = 0b1010001010;
	}else if(current_frame_seg == 5){
		data = 0b0101110101;
	}else if(current_frame_seg == 6){
		data = 0b1000101010;
	}else if(current_frame_seg == 7){
		data = 0b0111010101;
	}else if(current_frame_seg == 8){
		data = 0b0010101010;
	}else if(current_frame_seg == 9){
		data = 0b1101010101;
	}
	uint8_t cur_com = current_frame_seg / 2;
	for(uint8_t i = 0;i < 11;i++){
		if(current_frame_seg % 2 == 0){
			if(seg_data_enabled[i] & (1 << cur_com)){
				data |= (1 << (10 + i * 2)) | (1 << (11 + i * 2));
			}else{
				data |= (1 << (10 + i * 2));
				data &= ~(1 << (11 + i * 2));
			}
		}else{
			if(seg_data_enabled[i] & (1 << cur_com)){
				data &= ~(1 << (10 + i * 2)) & ~(1 << (11 + i * 2));
			}else{

				data &= ~(1 << (10 + i * 2));
				data |= (1 << (11 + i * 2));
			}
		}
	}

	/////////////////////////11
	if(current_frame_seg % 2 == 0){
		if(seg_data_enabled[11] & (1 << cur_com)){
			data2 |= (1 << (0)) | (1 << (1));
		}else{
			data2 |= (1 << (0));
			data2 &= ~(1 << (1));
		}
	}else{
		if(seg_data_enabled[11] & (1 << cur_com)){
			data2 &= ~(1 << (0)) & ~(1 << (1));
		}else{

			data2 &= ~(1 << (0));
			data2 |= (1 << (1));
		}
	}

	/////////////////////////12 14
	if(current_frame_seg % 2 == 0){
		if(seg_data_enabled[12] & (1 << cur_com)){
			COM_2_V3;
		}else{
			COM_2_V1;
		}
		if(seg_data_enabled[14] & (1 << cur_com)){
			COM_1_V3;
		}else{
			COM_1_V1;
		}
	}else{
		if(seg_data_enabled[12] & (1 << cur_com)){
			COM_2_V0;
		}else{
			COM_2_V2;
		}
		if(seg_data_enabled[14] & (1 << cur_com)){
			COM_1_V0;
		}else{
			COM_1_V2;
		}
	}

	/////////////////////////13
	if(current_frame_seg % 2 == 0){
		if(seg_data_enabled[13] & (1 << cur_com)){
			data2 |= (1 << (2)) | (1 << (3));
		}else{
			data2 |= (1 << (2));
			data2 &= ~(1 << (3));
		}
	}else{
		if(seg_data_enabled[13] & (1 << cur_com)){
			data2 &= ~(1 << (2)) & ~(1 << (3));
		}else{

			data2 &= ~(1 << (2));
			data2 |= (1 << (3));
		}
	}
     ///////////////////////////


	for(uint8_t i = 15;i< 29;i++){
		uint8_t i_shifted = i - 13;
		if(current_frame_seg % 2 == 0){
			if(seg_data_enabled[i] & (1 << cur_com)){
				data2 |= (1 << (i_shifted * 2)) | (1 << (1 + i_shifted * 2));
			}else{
				data2 |= (1 << (i_shifted * 2));
				data2 &= ~(1 << (1 + i_shifted * 2));
			}
		}else{
			if(seg_data_enabled[i] & (1 << cur_com)){
				data2 &= ~(1 << (i_shifted * 2)) & ~(1 << (1 + i_shifted * 2));
			}else{
				data2 &= ~(1 << (i_shifted * 2));
				data2 |= (1 << (1 + i_shifted * 2));

			}
		}
	}

	write_to_registers(data2,data);
	if(current_frame_seg == 9) current_frame_seg = 0;
	else current_frame_seg++;
}
static void set_symbol(Coord *sym){

	for(uint8_t i = 0;i < 3;i++){
		seg_data_enabled[sym[i] >> 5] |= sym[i];
	}
}
static void clear_all(void){
	for(uint8_t i = 0;i < 29;i++) seg_data_enabled[i]=0;
}
static void set_hours(uint8_t hours){
	set_symbol(hour_0[hours % 10]);
	uint8_t dec_h = ((uint8_t)(hours / 10));
	set_symbol(hour_1[dec_h]);
}
static void set_minutes(uint8_t minutes){
	set_symbol(minute_0[minutes % 10]);
	set_symbol(minute_1[((uint8_t)(minutes / 10))]);
}
static void set_seconds(uint8_t sec){
	set_symbol(second_0[sec % 10]);
	set_symbol(second_1[((uint8_t)(sec / 10))]);
}
static void set_day(uint8_t day){
	set_symbol(day_0[day % 10]);
	set_symbol(day_1[((uint8_t)(day / 10))]);
}
static void set_month(uint8_t month){
	set_symbol(month_0[month % 10]);
	set_symbol(month_1[((uint8_t)(month / 10))]);
}
static void set_dots(void){
	seg_data_enabled[16] |= 4;
}
static void set_date_slash(void){
	seg_data_enabled[3] |= 0b01000;
}
static void set_week_day(uint8_t day){
	if(day == 0){
		seg_data_enabled[5] |= 0b01000;
		seg_data_enabled[6] |= 0b00001;
		seg_data_enabled[7] |= 0b00001;
		seg_data_enabled[8] |= 0b11110;
		seg_data_enabled[9] |= 0b00100;
		seg_data_enabled[10] |= 0b11010;
	}
	if(day == 1){
		seg_data_enabled[5] |= 0b11000;
		seg_data_enabled[6] |= 0b00101;
		seg_data_enabled[7] |= 0b01001;
		seg_data_enabled[8] |= 0b10100;
		seg_data_enabled[9] |= 0b01011;
	}
	if(day == 2){
		seg_data_enabled[5] |= 0b11000;
		seg_data_enabled[6] |= 0b00001;
		seg_data_enabled[7] |= 0b00001;
		seg_data_enabled[8] |= 0b01010;
		seg_data_enabled[9] |= 0b00101;
		seg_data_enabled[10] |= 0b10010;
	}
	if(day == 3){
		seg_data_enabled[6] |= 0b00101;
		seg_data_enabled[7] |= 0b01000;
		seg_data_enabled[8] |= 0b10100;
		seg_data_enabled[9] |= 0b01011;
	}
	if(day == 4){
		seg_data_enabled[5] |= 0b01000;
		seg_data_enabled[6] |= 0b00001;
		seg_data_enabled[7] |= 0b00001;
		seg_data_enabled[8] |= 0b10100;
		seg_data_enabled[9] |= 0b01011;
	}
	if(day == 5){
		seg_data_enabled[5] |= 0b11000;
		seg_data_enabled[6] |= 0b00001;
		seg_data_enabled[7] |= 0b00001;
		seg_data_enabled[8] |= 0b01010;
		seg_data_enabled[9] |= 0b10101;
		seg_data_enabled[10] |= 0b01010;
	}
	if(day == 6){
		seg_data_enabled[5] |= 0b11000;
		seg_data_enabled[6] |= 0b00101;
		seg_data_enabled[7] |= 0b01001;
		seg_data_enabled[8] |= 0b11110;
		seg_data_enabled[9] |= 0b10001;
	}
}
//static void set_luna(){
//	uint8_t phase = 0b100001;
//	float percent = (((rtc_get_counter_val() - LAST_FULL_MOON) % MOON_CICLE_SEK) / (float)(MOON_CICLE_SEK - 1)) * 80;
//	if(percent <= 5 || percent > 75){
//		phase |= 0b111111;
//	}else if(percent <= 15 ){
//		phase |= 0b101111;
//	}else if(percent <= 25 ){
//		phase |= 0b100111;
//	}else if(percent <= 35 ){
//		phase |= 0b100011;
//	}else if(percent <= 45 ){
//		phase |= 0b100001;
//	}else if(percent <= 55 ){
//		phase |= 0b110001;
//	}else if(percent <= 65 ){
//		phase |= 0b111001;
//	}else if(percent <= 75 ) {
//		phase |= 0b111101;
//	}
//
//	seg_data_enabled[14] |= phase;
//	seg_data_enabled[11] |= phase << 4;
//}
static void set_temp_c(float c){
	seg_data_enabled[27] |= 1;
	seg_data_enabled[28] |= 0b10;
	set_symbol(temp_0[((uint8_t)c) % 10]);
	set_symbol(temp_1[((uint8_t)(c / 10))]);
	set_symbol(temp_d[((uint8_t)(c * 10)) % 10]);
}
static void recalc_display(void){
	struct tm global_time;
	xttotm(&global_time,rtc_get_counter_val());
	set_month(global_time.tm_mon);
	set_day(global_time.tm_mday);
	set_hours(global_time.tm_hour);
	set_minutes(global_time.tm_min);
	set_seconds(global_time.tm_sec);
	set_week_day(global_time.tm_wday);
	set_dots();
	if(rtc_get_counter_val() % 120 == 0){
		temp = get_temp();
	}
	set_temp_c(temp);
	//set_luna();
	set_date_slash();
}

static void usart_setup(void){
	rcc_periph_clock_enable(RCC_USART1);
	rcc_periph_clock_enable(RCC_GPIOA);
	gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_USART1_TX);

	usart_set_baudrate(USART1, 9600);
	usart_set_databits(USART1, 8);
	usart_set_stopbits(USART1, USART_STOPBITS_1);
	usart_set_mode(USART1, USART_MODE_TX);
	usart_set_parity(USART1, USART_PARITY_NONE);
	usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);

	usart_enable(USART1);
}

static void send_string(uint8_t *c){
	for(uint8_t i = 0;i < strlen((char *)c);i++){
		usart_send_blocking(USART1,c[i]);
	}
}

static void adc_start(void){

	rcc_periph_clock_enable(RCC_ADC1);

	adc_power_off(ADC1);

	adc_disable_scan_mode(ADC1);
	adc_set_single_conversion_mode(ADC1);
	adc_disable_external_trigger_regular(ADC1);
	adc_set_right_aligned(ADC1);
	adc_enable_temperature_sensor();
	adc_set_sample_time_on_all_channels(ADC1, ADC_SMPR_SMP_28DOT5CYC);
	adc_power_on(ADC1);
	for (uint32_t i = 0; i < 800000; i++) __asm__("nop");
	adc_reset_calibration(ADC1);
	adc_calibrate(ADC1);
	uint8_t channel_array[16];
	channel_array[0] = 16;
	adc_set_regular_sequence(ADC1, 1, channel_array);
}
static void adc_stop(void){
	adc_power_off(ADC1);
	adc_disable_temperature_sensor();
	rcc_periph_clock_disable(RCC_ADC1);
}
static float get_temp(void){
	float temp_1 = 23.7,temp_2 = 22.2;
	uint16_t temp_raw_1 = 1708,temp_raw_2 = 1716;
	float ppc = (temp_1 - temp_2) / (temp_raw_2 - temp_raw_1);
	adc_start();
	float measure_summ = 0;
	for(uint8_t i = 0;i < 10;i++){
		adc_start_conversion_direct(ADC1);
		while (!(adc_eoc(ADC1)));
		uint16_t tmp = adc_read_regular(ADC1);
		measure_summ += temp_2 - ((tmp - temp_raw_2) * ppc);
		delay_ms(1);
	}
	adc_stop();
	return measure_summ / 10.0;
}

int main(void){
	rcc_clock_setup_in_hse_8mhz_out_72mhz();
	delay_ms(10);
	rtc_setup();
	delay_ms(10);
	gpio_setup();
	write_to_registers(0,0);
	timer_setup();

	if(rtc_get_counter_val() == 0){
		rtc_set_counter_val(START_TIME);
	}

	usart_setup();
	while (1) {

		if(recalculate_screen){
			recalculate_screen = 0;
			clear_all();
			recalc_display();
		}
	}

	return 0;
}
